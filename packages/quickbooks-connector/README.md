# NestJS Based Resource Server

Add resource server to infrastructure

POST /setup to setup service
POST /info to get service info

Use to start app development for NestJS backend

```
[METHOD] /quickbooks/v1/command/{quickbooks_client_uuid}/{quickbook_request}
```

- `quickbooks_client_uuid`, uuid of client added on connector
- `quickbook_request`, request to be made to QBO using Bearer Token connected to the user and provided realm
