export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const COMMUNICATION_SERVICE_NOT_IMPLEMENTED =
  'Communication Server not implemented';
export const INVALID_STATE = 'Invalid State';
export const INVALID_QUICKBOOKS_CLIENT = 'Invalid QuickBooks Client';
export const INVALID_USER = 'Invalid User';
export const INVALID_QUICKBOOKS_TOKEN = 'Invalid QuickBooks Token';
export const NOT_CONNECTED = 'not connected';
export const INVALID_CODE = 'Invalid Code';
export const INVALID_QUICKBOOKS_REALM_ID = 'Invalid QuickBooks Realm ID';
