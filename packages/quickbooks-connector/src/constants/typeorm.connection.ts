import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
} from '../config/config.service';
import { ServerSettings } from '../system-settings/entities/server-settings/server-settings.entity';
import { TokenCache } from '../auth/entities/token-cache/token-cache.entity';
import { QuickBooksToken } from '../quickbooks-connector/entities/quickbooks-token/quickbooks-token.entity';
import { RequestState } from '../quickbooks-connector/entities/request-state/request-state.entity';
import { QuickBooksClient } from '../quickbooks-connector/entities/quickbooks-client/quickbooks-client.entity';
import { RequestLog } from '../quickbooks-connector/entities/request-log/request-log.entity';

export function connectTypeorm(config): MongoConnectionOptions {
  return {
    url: `mongodb://${config.get(DB_USER)}:${config.get(
      DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}?useUnifiedTopology=true`,
    type: 'mongodb',
    logging: false,
    synchronize: true,
    entities: [
      ServerSettings,
      TokenCache,
      QuickBooksToken,
      RequestState,
      QuickBooksClient,
      RequestLog,
    ],
    useNewUrlParser: true,
  };
}
