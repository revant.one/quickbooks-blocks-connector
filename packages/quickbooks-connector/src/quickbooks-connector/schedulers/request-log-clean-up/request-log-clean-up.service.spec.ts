import { Test, TestingModule } from '@nestjs/testing';
import { RequestLogCleanUpService } from './request-log-clean-up.service';
import { RequestLogService } from '../../entities/request-log/request-log.service';

describe('RequestLogCleanUpService', () => {
  let service: RequestLogCleanUpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RequestLogCleanUpService,
        { provide: RequestLogService, useValue: {} },
      ],
    }).compile();

    service = module.get<RequestLogCleanUpService>(RequestLogCleanUpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
