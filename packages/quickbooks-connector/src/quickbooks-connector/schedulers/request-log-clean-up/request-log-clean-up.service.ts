import { Injectable, OnModuleInit } from '@nestjs/common';
import { CronJob } from 'cron';
import { RequestLogService } from '../../entities/request-log/request-log.service';

export const REQUEST_LOG_CLEANUP_CRON_STRING = '0 */30 * * * *';

@Injectable()
export class RequestLogCleanUpService implements OnModuleInit {
  constructor(private readonly requestLog: RequestLogService) {}
  onModuleInit() {
    const job = new CronJob(REQUEST_LOG_CLEANUP_CRON_STRING, async () => {
      await this.requestLog.deleteMany({
        expiration: { $lte: new Date() },
      });
    });
    job.start();
  }
}
