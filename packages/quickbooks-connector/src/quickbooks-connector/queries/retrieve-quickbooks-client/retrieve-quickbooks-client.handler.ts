import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { RetrieveQuickBooksClientQuery } from './retrieve-quickbooks-client.query';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@QueryHandler(RetrieveQuickBooksClientQuery)
export class RetrieveQuickBooksClientHandler
  implements IQueryHandler<RetrieveQuickBooksClientQuery> {
  constructor(private manager: QuickBooksClientAggregateService) {}

  async execute(query: RetrieveQuickBooksClientQuery) {
    const { uuid } = query;
    return await this.manager.retrieveProvider(uuid);
  }
}
