import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListQuickBooksClientQuery } from './list-quickbooks-client.query';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@QueryHandler(ListQuickBooksClientQuery)
export class ListQuickBooksClientHandler
  implements IQueryHandler<ListQuickBooksClientQuery> {
  constructor(private manager: QuickBooksClientAggregateService) {}

  async execute(query: ListQuickBooksClientQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.list(offset, limit, search, sort);
  }
}
