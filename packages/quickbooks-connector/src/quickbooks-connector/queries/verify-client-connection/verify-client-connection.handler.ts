import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { VerifyClientConnectionQuery } from './verify-client-connection.query';
import { QuickBooksTokenManagerService } from '../../aggregates/quickbooks-token-manager/quickbooks-token-manager.service';
import { BadRequestException } from '@nestjs/common';
import {
  INVALID_USER,
  INVALID_QUICKBOOKS_CLIENT,
  INVALID_QUICKBOOKS_REALM_ID,
} from '../../../constants/messages';

@QueryHandler(VerifyClientConnectionQuery)
export class VerifyClientConnectionHandler
  implements IQueryHandler<VerifyClientConnectionQuery> {
  constructor(private manager: QuickBooksTokenManagerService) {}

  async execute(query: VerifyClientConnectionQuery) {
    const { providerUuid, userUuid, realmId } = query;
    if (!userUuid) throw new BadRequestException(INVALID_USER);
    if (!providerUuid) throw new BadRequestException(INVALID_QUICKBOOKS_CLIENT);
    if (!realmId) throw new BadRequestException(INVALID_QUICKBOOKS_REALM_ID);
    const token = await this.manager.verifyClientConnection(
      providerUuid,
      userUuid,
      realmId,
    );
    const isConnected = token ? true : false;
    return { isConnected };
  }
}
