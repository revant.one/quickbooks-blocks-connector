import { ListQuickBooksClientHandler } from './list-quickbooks-client/list-quickbooks-client.handler';
import { ListRequestUserLogHandler } from './list-user-request-log/list-user-request-log.handler';
import { ListRequestAdminLogHandler } from './list-request-log/list-request-log.handler';
import { RetrieveQuickBooksClientHandler } from './retrieve-quickbooks-client/retrieve-quickbooks-client.handler';
import { RetrieveRequestLogHandler } from './retrieve-request-log/retrieve-request-log.handler';
import { VerifyClientConnectionHandler } from './verify-client-connection/verify-client-connection.handler';

export const QuickBooksQueryHandlers = [
  ListQuickBooksClientHandler,
  ListRequestAdminLogHandler,
  ListRequestUserLogHandler,
  RetrieveQuickBooksClientHandler,
  RetrieveRequestLogHandler,
  VerifyClientConnectionHandler,
];
