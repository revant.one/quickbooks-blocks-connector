import { IQuery } from '@nestjs/cqrs';

export class ListUserRequestLogQuery implements IQuery {
  constructor(
    public readonly offset: number,
    public readonly limit: number,
    public readonly uuid: string,
    public readonly search?: string,
    public readonly sort?: string,
  ) {}
}
