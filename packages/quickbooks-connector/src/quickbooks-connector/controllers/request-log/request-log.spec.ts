import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { QuickBooksTokenManagerService } from '../../aggregates/quickbooks-token-manager/quickbooks-token-manager.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { HttpService } from '@nestjs/common';
import { RequestLogController } from './request-log.controller';

describe('Request Log Controller', () => {
  let controller: RequestLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [RequestLogController],
      providers: [
        { provide: QuickBooksTokenManagerService, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: HttpService, useFactory: (...args) => jest.fn() },
      ],
    }).compile();

    controller = module.get<RequestLogController>(RequestLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
