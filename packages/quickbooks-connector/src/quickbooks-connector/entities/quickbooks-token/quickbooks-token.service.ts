import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { QuickBooksToken } from './quickbooks-token.entity';

@Injectable()
export class QuickBooksTokenService {
  constructor(
    @InjectRepository(QuickBooksToken)
    private readonly tokenRepository: MongoRepository<QuickBooksToken>,
  ) {}

  async save(params) {
    return await this.tokenRepository.save(params);
  }

  async find(params?): Promise<QuickBooksToken[]> {
    return await this.tokenRepository.find(params);
  }

  async findOne(params): Promise<QuickBooksToken> {
    return await this.tokenRepository.findOne(params);
  }

  async update(query, params) {
    return await this.tokenRepository.update(query, params);
  }

  async count() {
    return await this.tokenRepository.count();
  }

  async paginate(skip: number, take: number) {
    return await this.tokenRepository.find({ skip, take });
  }

  async deleteMany(params) {
    return await this.tokenRepository.deleteMany(params);
  }

  async list(skip: number, take: number, search?: string, sort?: string) {
    const providers = await this.tokenRepository.find({ skip, take });

    return {
      docs: providers,
      length: await this.tokenRepository.count(),
      offset: skip,
    };
  }
}
