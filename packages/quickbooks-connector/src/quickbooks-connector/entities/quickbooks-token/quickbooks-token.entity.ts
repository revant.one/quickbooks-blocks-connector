import { Entity, ObjectIdColumn, ObjectID, Column, BaseEntity } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

export const QUICKBOOKS_TOKEN = 'quickbooks_token';

@Entity({ name: QUICKBOOKS_TOKEN })
export class QuickBooksToken extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  providerUuid: string;

  @Column()
  userUuid: string;

  @Column()
  accessToken: string;

  @Column()
  refreshToken: string;

  @Column()
  idToken: string;

  @Column()
  expirationTime: Date;

  @Column()
  realmId: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
