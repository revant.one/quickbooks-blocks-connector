import { Test, TestingModule } from '@nestjs/testing';
import { RequestLogService } from './request-log.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RequestLog } from './request-log.entity';

describe('RequestLogService', () => {
  let service: RequestLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RequestLogService,
        { provide: getRepositoryToken(RequestLog), useValue: {} },
      ],
    }).compile();

    service = module.get<RequestLogService>(RequestLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
