import { Column, Entity, BaseEntity, ObjectID, ObjectIdColumn } from 'typeorm';
import * as uuidv4 from 'uuid/v4';
import { QuickBooksEnvironment } from './quickbooks-client-environment.enum';

export const QUICKBOOKS_CLIENT = 'quickbooks_client';

@Entity({ name: QUICKBOOKS_CLIENT })
export class QuickBooksClient extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  clientId: string;

  @Column()
  clientSecret: string;

  @Column()
  scope: string[];

  @Column()
  environment: QuickBooksEnvironment;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
