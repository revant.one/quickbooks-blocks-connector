import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { QuickBooksClient } from './quickbooks-client.entity';
import { QuickBooksClientService } from './quickbooks-client.service';

describe('QuickBooksClientService', () => {
  let service: QuickBooksClientService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuickBooksClientService,
        {
          provide: getRepositoryToken(QuickBooksClient),
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<QuickBooksClientService>(QuickBooksClientService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
