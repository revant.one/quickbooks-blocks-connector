import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { QuickBooksClient } from './quickbooks-client.entity';

@Injectable()
export class QuickBooksClientService {
  constructor(
    @InjectRepository(QuickBooksClient)
    private readonly clientRepository: MongoRepository<QuickBooksClient>,
  ) {}

  async save(params) {
    return await this.clientRepository.save(params);
  }

  async find(): Promise<QuickBooksClient[]> {
    return await this.clientRepository.find();
  }

  async findOne(params): Promise<QuickBooksClient> {
    return await this.clientRepository.findOne(params);
  }

  async update(query, params) {
    return await this.clientRepository.update(query, params);
  }

  async count() {
    return await this.clientRepository.count();
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.clientRepository.manager.connection
      .getMetadata(QuickBooksClient)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.clientRepository.find({
      skip,
      take,
      where,
    });

    const length = await this.clientRepository.count(where);

    return {
      docs: results || [],
      length,
      offset: skip,
    };
  }
}
