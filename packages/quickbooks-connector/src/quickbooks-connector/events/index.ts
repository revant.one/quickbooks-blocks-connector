import { QuickBooksClientAddedHandler } from './quickbooks-client-added/quickbooks-client-added.handler';
import { QuickBooksClientRemovedHandler } from './quickbooks-client-removed/quickbooks-client-removed.handler';
import { QuickBooksClientUpdatedHandler } from './quickbooks-client-updated/quickbooks-client-updated.handler';

export const QuickBooksEventHandlers = [
  QuickBooksClientAddedHandler,
  QuickBooksClientRemovedHandler,
  QuickBooksClientUpdatedHandler,
];
