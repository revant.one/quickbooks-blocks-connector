import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuickBooksClientRemovedEvent } from './quickbooks-client-removed.event';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';

@EventsHandler(QuickBooksClientRemovedEvent)
export class QuickBooksClientRemovedHandler
  implements IEventHandler<QuickBooksClientRemovedEvent> {
  constructor(private readonly token: QuickBooksTokenService) {}

  handle(event: QuickBooksClientRemovedEvent) {
    const { client: provider } = event;
    this.token
      .find({ providerUuid: provider.uuid })
      .then(tokens => {
        for (const token of tokens) {
          token
            .remove()
            .then(success => {})
            .catch(error => {});
        }
      })
      .catch(error => {});

    provider
      .remove()
      .then(success => {})
      .catch(error => {});
  }
}
