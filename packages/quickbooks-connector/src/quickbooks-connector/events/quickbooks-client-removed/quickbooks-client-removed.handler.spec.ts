import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { QuickBooksClientRemovedHandler } from './quickbooks-client-removed.handler';
import { QuickBooksClientRemovedEvent } from './quickbooks-client-removed.event';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';

describe('Event: QuickBooksClientRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: QuickBooksClientRemovedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        QuickBooksClientRemovedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: QuickBooksTokenService,
          useValue: {
            find: (...args) => Promise.resolve([]),
          },
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<QuickBooksClientRemovedHandler>(
      QuickBooksClientRemovedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove OAuth2Provider', async () => {
    const mockProvider = new QuickBooksClient();
    eventBus$.publish = jest.fn(() => {});
    mockProvider.remove = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new QuickBooksClientRemovedEvent(mockProvider));
    expect(mockProvider.remove).toHaveBeenCalledTimes(1);
  });
});
