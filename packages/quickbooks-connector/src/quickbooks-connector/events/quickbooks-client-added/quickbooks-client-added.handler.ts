import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuickBooksClientAddedEvent } from './quickbooks-client-added.event';

@EventsHandler(QuickBooksClientAddedEvent)
export class QuickBooksClientAddedHandler
  implements IEventHandler<QuickBooksClientAddedEvent> {
  handle(event: QuickBooksClientAddedEvent) {
    const { client: provider } = event;
    provider
      .save()
      .then(success => {})
      .catch(error => {});
  }
}
