import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { QuickBooksClientAddedHandler } from './quickbooks-client-added.handler';
import { QuickBooksClientAddedEvent } from './quickbooks-client-added.event';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';

describe('Event: QuickBooksClientAddedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: QuickBooksClientAddedHandler;
  const mockProvider = new QuickBooksClient();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        QuickBooksClientAddedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<QuickBooksClientAddedHandler>(
      QuickBooksClientAddedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    mockProvider.save = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new QuickBooksClientAddedEvent(mockProvider));
    expect(mockProvider.save).toHaveBeenCalledTimes(1);
  });
});
