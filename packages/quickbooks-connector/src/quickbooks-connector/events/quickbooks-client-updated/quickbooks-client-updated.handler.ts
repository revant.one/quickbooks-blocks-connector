import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { QuickBooksClientUpdatedEvent } from './quickbooks-client-updated.event';

@EventsHandler(QuickBooksClientUpdatedEvent)
export class QuickBooksClientUpdatedHandler
  implements IEventHandler<QuickBooksClientUpdatedEvent> {
  handle(event: QuickBooksClientUpdatedEvent) {
    const { client: provider } = event;
    provider
      .save()
      .then(success => {})
      .catch(error => {});
  }
}
