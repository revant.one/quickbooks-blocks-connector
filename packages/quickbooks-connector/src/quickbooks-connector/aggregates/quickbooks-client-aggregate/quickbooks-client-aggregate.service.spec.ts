import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/common';
import { QuickBooksClientAggregateService } from './quickbooks-client-aggregate.service';
import { QuickBooksClientService } from '../../entities/quickbooks-client/quickbooks-client.service';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { QuickBooksConnectionService } from '../quick-books-connection/quick-books-connection.service';

describe('QuickBooksClientAggregateService', () => {
  let service: QuickBooksClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: QuickBooksClientAggregateService,
          useValue: {},
        },
        { provide: QuickBooksClientService, useValue: {} },
        { provide: QuickBooksTokenService, useValue: {} },
        { provide: HttpService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
        { provide: QuickBooksConnectionService, useValue: {} },
      ],
    }).compile();

    service = module.get<QuickBooksClientAggregateService>(
      QuickBooksClientAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
