import {
  Injectable,
  HttpService,
  NotImplementedException,
  BadRequestException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { switchMap, map } from 'rxjs/operators';
import { throwError, of, from } from 'rxjs';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  COMMUNICATION_SERVICE_NOT_IMPLEMENTED,
  INVALID_STATE,
  INVALID_QUICKBOOKS_CLIENT,
} from '../../../constants/messages';
import { RequestStateService } from '../../entities/request-state/request-state.service';
import { QuickBooksTokenService } from '../../entities/quickbooks-token/quickbooks-token.service';
import { RequestState } from '../../entities/request-state/request-state.entity';
import { QuickBooksClientService } from '../../entities/quickbooks-client/quickbooks-client.service';
import { stringify } from 'querystring';
import { QuickBooksClient } from '../../entities/quickbooks-client/quickbooks-client.entity';
import { QuickBooksConnectionService } from '../quick-books-connection/quick-books-connection.service';
import {
  ACCEPT_HEADER_KEY,
  APPLICATION_JSON_CONTENT_TYPE,
  CONTENT_TYPE_HEADER_KEY,
  APPLICATION_X_WWW_FORM_URLENCODED,
} from '../../../constants/app-strings';

export const REDIRECT_ENDPOINT = '/quickbooks/callback';

@Injectable()
export class QuickBooksTokenManagerService {
  private client: QuickBooksClient;
  private localState = new RequestState();

  constructor(
    private readonly http: HttpService,
    private readonly settings: SettingsService,
    private readonly requestState: RequestStateService,
    private readonly token: QuickBooksTokenService,
    private readonly quickBooksClient: QuickBooksClientService,
    private readonly connection: QuickBooksConnectionService,
  ) {}

  connectClientForUser(uuid: string, redirect: string, req) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.communicationService) {
          return throwError(
            new NotImplementedException(COMMUNICATION_SERVICE_NOT_IMPLEMENTED),
          );
        }

        return from(this.quickBooksClient.findOne({ uuid })).pipe(
          switchMap(client => {
            if (!client)
              return throwError(
                new NotFoundException(INVALID_QUICKBOOKS_CLIENT),
              );
            this.client = client;
            return from(
              this.requestState.save({
                uuid: uuidv4(),
                redirect,
                userUuid: req.token.sub,
                providerUuid: uuid,
                creation: new Date(),
              }),
            );
          }),
          switchMap(state => {
            const encodedState = state.uuid;
            let redirectTo =
              this.connection.getAuthorizationUrl() +
              '?client_id=' +
              this.client.clientId;
            redirectTo +=
              '&redirect_uri=' +
              encodeURIComponent(settings.appURL + REDIRECT_ENDPOINT);
            redirectTo += '&scope=' + this.client.scope.join('%20');
            redirectTo += '&response_type=code';
            redirectTo += '&state=' + encodedState;
            return of({ redirect: redirectTo });
          }),
        );
      }),
    );
  }

  processCode(code: string, state: string, realmId: string, res) {
    const headers = {};
    headers[ACCEPT_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_X_WWW_FORM_URLENCODED;

    this.settings
      .find()
      .pipe(
        switchMap(settings => {
          return from(this.requestState.findOne({ uuid: state })).pipe(
            switchMap(requestState => {
              if (!requestState) {
                return throwError(new BadRequestException(INVALID_STATE));
              }

              this.localState = requestState;

              return from(
                this.quickBooksClient.findOne({
                  uuid: this.localState.providerUuid,
                }),
              ).pipe(
                switchMap(client => {
                  const requestBody = {
                    code,
                    grant_type: 'authorization_code',
                    redirect_uri: settings.appURL + REDIRECT_ENDPOINT,
                  };

                  return this.http.post(
                    this.connection.getTokenUrl(),
                    stringify(requestBody),
                    {
                      headers,
                      auth: {
                        username: client.clientId,
                        password: client.clientSecret,
                      },
                    },
                  );
                }),
              );
            }),
          );
        }),
        map(response => response.data),
        switchMap(token => {
          return from(
            this.token.findOne({
              providerUuid: this.localState.providerUuid,
              userUuid: this.localState.userUuid,
              realmId,
            }),
          ).pipe(
            switchMap(localToken => {
              // Set Saved Token Expiration Time
              const expirationTime = new Date();
              expirationTime.setSeconds(
                expirationTime.getSeconds() + (token.expires_in || 3600),
              );

              if (!localToken) {
                return from(
                  this.token.save({
                    uuid: uuidv4(),
                    providerUuid: this.localState.providerUuid,
                    userUuid: this.localState.userUuid,
                    accessToken: token.access_token,
                    refreshToken: token.refresh_token,
                    idToken: token.id_token,
                    expirationTime,
                    realmId,
                  }),
                );
              }

              this.revokeToken(
                localToken.accessToken,
                this.localState.providerUuid,
              );
              localToken.uuid = localToken.uuid;
              localToken.providerUuid = this.localState.providerUuid;
              localToken.userUuid = this.localState.userUuid;
              localToken.accessToken = token.access_token;
              localToken.refreshToken = token.refresh_token;
              localToken.idToken = token.id_token;
              localToken.expirationTime = expirationTime;
              return from(localToken.save());
            }),
          );
        }),
      )
      .subscribe({
        next: response => {
          const redirect = this.localState.redirect || '/';

          this.deleteRequestState(this.localState);

          return res.redirect(HttpStatus.FOUND, redirect);
        },
        error: error => {
          res.status(HttpStatus.INTERNAL_SERVER_ERROR);
          return res.json({ error: error.message });
        },
      });
  }

  revokeToken(accessToken: string, providerUuid: string) {
    const headers = {};
    headers[ACCEPT_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;

    from(this.quickBooksClient.findOne({ uuid: providerUuid }))
      .pipe(
        switchMap(client => {
          return this.http.post(
            this.connection.getRevocationUrl(),
            { token: accessToken },
            {
              headers,
              auth: {
                username: client.clientId,
                password: client.clientSecret,
              },
            },
          );
        }),
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }

  deleteRequestState(requestState: RequestState) {
    from(requestState.remove()).subscribe({
      next: success => {},
      error: error => {},
    });
  }

  async verifyClientConnection(
    providerUuid: string,
    userUuid: string,
    realmId: string,
  ) {
    return await this.token.findOne({ userUuid, providerUuid, realmId });
  }

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(offset);
    return this.token.list(offset, limit, search, sort);
  }
}
