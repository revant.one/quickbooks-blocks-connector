import { Test, TestingModule } from '@nestjs/testing';
import { QuickBooksConnectionService } from './quick-books-connection.service';
import { HttpService } from '@nestjs/common';

describe('QuickBooksConnectionService', () => {
  let service: QuickBooksConnectionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuickBooksConnectionService,
        { provide: HttpService, useFactory: jest.fn() },
      ],
    }).compile();

    service = module.get<QuickBooksConnectionService>(
      QuickBooksConnectionService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
