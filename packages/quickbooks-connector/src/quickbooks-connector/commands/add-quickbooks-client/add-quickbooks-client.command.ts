import { ICommand } from '@nestjs/cqrs';
import { QuickBooksClientDto } from '../../policies/quickbooks-client-dto/quickbooks-client.dto';

export class AddQuickBooksClientCommand implements ICommand {
  constructor(public readonly payload: QuickBooksClientDto) {}
}
