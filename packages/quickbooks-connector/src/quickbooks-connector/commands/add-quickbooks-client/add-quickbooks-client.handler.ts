import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddQuickBooksClientCommand } from './add-quickbooks-client.command';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@CommandHandler(AddQuickBooksClientCommand)
export class AddQuickBooksClientHandler
  implements ICommandHandler<AddQuickBooksClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuickBooksClientAggregateService,
  ) {}

  async execute(command: AddQuickBooksClientCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addProvider(payload);
    aggregate.commit();
  }
}
