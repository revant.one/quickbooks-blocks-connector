import { ICommand } from '@nestjs/cqrs';
import { QuickBooksClientDto } from '../../policies/quickbooks-client-dto/quickbooks-client.dto';

export class UpdateQuickBooksClientCommand implements ICommand {
  constructor(
    public readonly providerUuid: string,
    public readonly payload: QuickBooksClientDto,
  ) {}
}
