import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateQuickBooksClientCommand } from './update-quickbooks-client.command';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@CommandHandler(UpdateQuickBooksClientCommand)
export class UpdateQuickBooksClientHandler
  implements ICommandHandler<UpdateQuickBooksClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuickBooksClientAggregateService,
  ) {}

  async execute(command: UpdateQuickBooksClientCommand) {
    const { providerUuid, payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateProvider(providerUuid, payload);
    aggregate.commit();
  }
}
