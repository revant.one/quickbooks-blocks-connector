import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RefreshClientTokenCommand } from './refresh-client-token.command';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';
import { BadRequestException } from '@nestjs/common';
import { INVALID_QUICKBOOKS_TOKEN } from '../../../constants/messages';

@CommandHandler(RefreshClientTokenCommand)
export class RefreshClientTokenHandler
  implements ICommandHandler<RefreshClientTokenCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: QuickBooksClientAggregateService,
  ) {}

  async execute(command: RefreshClientTokenCommand) {
    const { providerUuid, userUuid, realmId } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const token = await this.manager.findOneToken(
      providerUuid,
      userUuid,
      realmId,
    );
    if (!token) throw new BadRequestException(INVALID_QUICKBOOKS_TOKEN);
    await aggregate.refreshToken(token);
  }
}
