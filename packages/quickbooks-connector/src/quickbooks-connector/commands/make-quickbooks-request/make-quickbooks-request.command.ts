import { ICommand } from '@nestjs/cqrs';
import { HttpMethod } from '../../../constants/request-methods';

export class MakeQuickBooksRequestCommand implements ICommand {
  constructor(
    public readonly headers: any,
    public readonly requestMethod: HttpMethod,
    public readonly clientUuid: string,
    public readonly params: any,
    public readonly query: any,
    public readonly payload: any,
    public readonly userUuid: string,
    public readonly paramString: string,
  ) {}
}
