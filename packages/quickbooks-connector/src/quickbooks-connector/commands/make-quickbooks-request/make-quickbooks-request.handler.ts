import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { MakeQuickBooksRequestCommand } from './make-quickbooks-request.command';
import { QuickBooksClientAggregateService } from '../../aggregates/quickbooks-client-aggregate/quickbooks-client-aggregate.service';

@CommandHandler(MakeQuickBooksRequestCommand)
export class MakeQuickBooksRequestHandler
  implements ICommandHandler<MakeQuickBooksRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: QuickBooksClientAggregateService,
  ) {}

  async execute(command: MakeQuickBooksRequestCommand) {
    const {
      headers,
      requestMethod,
      clientUuid,
      params,
      query,
      payload,
      userUuid,
      paramString,
    } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);

    const requestLog = await aggregate.makeRequest(
      headers,
      requestMethod,
      clientUuid,
      params,
      query,
      payload,
      userUuid,
      paramString,
    );

    return requestLog;
  }
}
