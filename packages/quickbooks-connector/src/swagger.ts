import { INestApplication } from '@nestjs/common';
import { readFileSync } from 'fs';
import { join } from 'path';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  APP_NAME,
  SWAGGER_ROUTE,
  SWAGGER_SCHEME,
} from './constants/app-strings';

export function setupSwagger(app: INestApplication) {
  const packageJson = JSON.parse(
    readFileSync(join(process.cwd(), 'package.json'), 'utf-8'),
  );
  const version = packageJson.version;
  const description = packageJson.description;
  const options = new DocumentBuilder()
    .setTitle(APP_NAME)
    .setDescription(description)
    .setVersion(version)
    .setSchemes(SWAGGER_SCHEME)
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(SWAGGER_ROUTE, app, document);
}
