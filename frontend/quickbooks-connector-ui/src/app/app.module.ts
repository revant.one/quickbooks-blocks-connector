import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { QuickBooksConnectorUIModule } from './quickbooks-connector-ui/quickbooks-connector-ui.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AccessTokenInjector } from './common/interceptors/access-token-injector/access-token-injector.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OAuthModule.forRoot(),
    SharedUIModule,
    QuickBooksConnectorUIModule,
  ],
  providers: [
    AppService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AccessTokenInjector,
      multi: true,
    },
    {
      provide: OAuthStorage,
      useValue: localStorage,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
