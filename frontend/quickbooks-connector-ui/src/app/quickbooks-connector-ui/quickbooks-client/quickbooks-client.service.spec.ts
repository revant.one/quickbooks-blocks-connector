import { TestBed } from '@angular/core/testing';

import { QuickBooksClientService } from './quickbooks-client.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../common/services/storage/storage.service';

describe('QuickBooksClientService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: StorageService, useValue: {} }],
    }),
  );

  it('should be created', () => {
    const service: QuickBooksClientService = TestBed.get(
      QuickBooksClientService,
    );
    expect(service).toBeTruthy();
  });
});
