import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../common/services/storage/storage.service';
import { APP_URL } from '../../constants/storage';

export const GET_QUICKBOOKS_CLIENT_ENDPOINT = '/quickbooks/v1/get/';
export const CREATE_QUICKBOOKS_CLIENT_ENDPOINT =
  '/quickbooks/v1/connect_client';
export const UPDATE_QUICKBOOKS_CLIENT_ENDPOINT =
  '/quickbooks/v1/update_client/';

@Injectable({
  providedIn: 'root',
})
export class QuickBooksClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getClient(uuid: string) {
    const appURL = this.storage.getInfo(APP_URL);
    const requestURL = appURL + GET_QUICKBOOKS_CLIENT_ENDPOINT + uuid;
    return this.http.get<any>(requestURL);
  }

  createClient(
    name: string,
    clientId: string,
    clientSecret: string,
    scope: string[],
    environment: string,
  ) {
    const client = {
      name,
      clientId,
      clientSecret,
      scope,
      environment,
    };

    const appURL = this.storage.getInfo(APP_URL);
    const requestUrl = appURL + CREATE_QUICKBOOKS_CLIENT_ENDPOINT;
    return this.http.post(requestUrl, client);
  }

  updateClient(
    uuid: string,
    name: string,
    clientId: string,
    clientSecret: string,
    scope: string[],
    environment: string,
  ) {
    const client = {
      name,
      clientId,
      clientSecret,
      scope,
      environment,
    };

    const appURL = this.storage.getInfo(APP_URL);
    const requestUrl = appURL + UPDATE_QUICKBOOKS_CLIENT_ENDPOINT + uuid;
    return this.http.post(requestUrl, client);
  }
}
