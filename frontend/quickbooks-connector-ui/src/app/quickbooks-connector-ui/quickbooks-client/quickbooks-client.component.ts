import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray } from '@angular/forms';
import {
  NEW_ID,
  DURATION,
  QuickBooksEnvironment,
  PRODUCTION,
  SANDBOX,
} from '../../constants/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { QuickBooksClientService } from './quickbooks-client.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
} from '../../constants/messages';
import { MatSnackBar } from '@angular/material';

export const QUICKBOOKS_CLIENT_LIST_ROUTE = '/list/quickbooks';

@Component({
  selector: 'app-quickbooks-client',
  templateUrl: './quickbooks-client.component.html',
  styleUrls: ['./quickbooks-client.component.css'],
})
export class QuickBooksClientComponent implements OnInit {
  clientName: string;
  clientId: string;
  clientSecret: string;
  scopeForms: FormArray;
  scope: string[];
  uuid: string;
  model: string;
  hideClientSecret: boolean = true;
  environment: string;

  clientEnvironment = [
    { value: QuickBooksEnvironment.SANDBOX, description: SANDBOX },
    { value: QuickBooksEnvironment.PRODUCTION, description: PRODUCTION },
  ];

  clientForm = new FormGroup({
    clientName: new FormControl(this.clientName),
    clientId: new FormControl(this.clientId),
    clientSecret: new FormControl(this.clientSecret),
    scopeForms: new FormArray([]),
    clientEnvironment: new FormControl(this.environment),
  });

  constructor(
    private router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly service: QuickBooksClientService,
    private snackBar: MatSnackBar,
  ) {
    this.uuid = this.activatedRoute.snapshot.params.id;
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[2];
      });
  }

  ngOnInit() {
    if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    } else {
      this.subscribeQuickBooksClient(this.uuid);
    }
  }

  createScopesFormGroup(scope?: string): FormGroup {
    return new FormGroup({ scope: new FormControl(scope) });
  }

  addScope(scope?: string) {
    this.scopeForms = this.clientForm.get('scopeForms') as FormArray;
    this.scopeForms.push(this.createScopesFormGroup(scope));
  }

  removeScope(formGroupID: number) {
    this.scopeForms.removeAt(formGroupID);
  }

  subscribeQuickBooksClient(uuid: string) {
    this.service.getClient(uuid).subscribe({
      next: response => {
        this.clientName = response.name;
        this.clientId = response.clientId;
        this.scope = response.scope;
        this.environment = response.environment;
        if (!this.environment) {
          this.environment = QuickBooksEnvironment.SANDBOX;
        }
        this.clientForm.controls.clientName.setValue(response.name);
        this.clientForm.controls.clientId.setValue(response.clientId);
        this.clientForm.controls.clientSecret.setValue(response.clientSecret);
        this.clientForm.controls.clientEnvironment.setValue(this.environment);
        this.scope.forEach(element => {
          this.addScope(element);
        });
      },
      error: error => {},
    });
  }

  createClient() {
    this.service
      .createClient(
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.clientId.value,
        this.clientForm.controls.clientSecret.value,
        this.getScopes(),
        this.clientForm.controls.clientEnvironment.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(QUICKBOOKS_CLIENT_LIST_ROUTE);
        },
        error: error => {
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  updateClient() {
    this.service
      .updateClient(
        this.uuid,
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.clientId.value,
        this.clientForm.controls.clientSecret.value,
        this.getScopes(),
        this.clientForm.controls.clientEnvironment.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(QUICKBOOKS_CLIENT_LIST_ROUTE);
        },
        error: error => {
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  getScopes(): string[] {
    const scopeFormsGroup = this.clientForm.get('scopeForms') as FormArray;
    const scope: string[] = [];
    for (const control of scopeFormsGroup.controls) {
      scope.push(control.value.scope);
    }
    return scope;
  }
}
