import { Injectable } from '@angular/core';
import { StorageService } from '../../common/services/storage/storage.service';
import { HttpClient } from '@angular/common/http';
import { APP_URL } from '../../constants/storage';

export const GET_REQUEST_LOG_ENDPOINT = '/request_log/v1/get/';

@Injectable({
  providedIn: 'root',
})
export class RequestLogService {
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getRequestLog(uuid: string) {
    const appURL = this.storage.getInfo(APP_URL);
    return this.http.get<any>(appURL + GET_REQUEST_LOG_ENDPOINT + uuid);
  }
}
