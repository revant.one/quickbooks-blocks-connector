import { TestBed } from '@angular/core/testing';

import { RequestLogService } from './request-log.service';
import { StorageService } from '../../common/services/storage/storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('RequestLogService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: StorageService, useValue: {} }],
      imports: [HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: RequestLogService = TestBed.get(RequestLogService);
    expect(service).toBeTruthy();
  });
});
